// gtest
#include <gtest/gtest.h>   // googletest header file

// cc library
#include <engine.h>
#include <orangemonkey_ai.h>

// Testing fixture
struct PredefinedGameEngineTest : testing::Test {

  std::unique_ptr<othello::OthelloGameEngine> engine;

  PredefinedGameEngineTest();
  ~PredefinedGameEngineTest() override;
};


PredefinedGameEngineTest::PredefinedGameEngineTest()
{
  // fixture initialization
}

PredefinedGameEngineTest::~PredefinedGameEngineTest()
{
  // fixture teardown
}


TEST_F(PredefinedGameEngineTest, InitialCurrentPlayerIdIsOne)
{
  engine->initPlayerType<othello::HumanPlayer, othello::PlayerId::One>();
  engine->initPlayerType<othello::monkey_ais::OrangeMonkeyAI,
                         othello::PlayerId::Two>();

  EXPECT_EQ(engine->currentPlayerId(), othello::PlayerId::One);
}
