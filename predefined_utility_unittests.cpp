// gtest
#include <gtest/gtest.h>   // googletest header file

// cc library
#include <engine.h>
#include <orangemonkey_ai.h>

using BitPos  = othello::BitPos;
using MoveDir = othello::MoveDirection;

// Testing fixture
struct UtilityBoardTestParams {
  BitPos  pos;
  MoveDir dir;
  BitPos  expected_next_pos;
};

struct UtilityBoardTest : testing::Test, testing::WithParamInterface<UtilityBoardTestParams> {
  UtilityBoardTest() {}
  ~UtilityBoardTest() override;
};
UtilityBoardTest::~UtilityBoardTest() {}


TEST_P(UtilityBoardTest, NextPosition)
{
  auto params = GetParam();

  auto next_pos = othello::utility::nextPosition(params.pos,params.dir);
  EXPECT_EQ(params.expected_next_pos,next_pos);
}

INSTANTIATE_TEST_CASE_P(
  ValidMoves, UtilityBoardTest,
  testing::Values(UtilityBoardTestParams{BitPos(0), MoveDir::N, BitPos(8)},
                  UtilityBoardTestParams{BitPos(9), MoveDir::S, BitPos(1)}), );

INSTANTIATE_TEST_CASE_P(
  InvalidMoves, UtilityBoardTest,
  testing::Values(
    UtilityBoardTestParams{BitPos(1), MoveDir::S, BitPos::invalid()},
    UtilityBoardTestParams{BitPos(61), MoveDir::N, BitPos::invalid()}), );
